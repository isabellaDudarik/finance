import Organization.*;
import generated.Generate;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the amount of money that you want to change(UAH): ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Enter the currency to convert (USD or EUR or RUB): ");
        String currency = scan.nextLine();

        Bank bank = Generate.getBank();
        Exchanger exchanger = Generate.getExchanger();

        System.out.println("the Best exchange: ");
        if (bank.converter(amount, currency) > exchanger.converter(amount, currency)) {
            System.out.println(String.format("%s: %s UAH + commission = %s USD", bank.name, amount, bank.converter(amount, currency)));
        } else
            System.out.println(String.format("%s: %s UAH  = %s USD", exchanger.name, amount, exchanger.converter(amount, currency)));


        System.out.println("Enter the amount of money that you want to borrow(UAH): ");
        int amountForCredit = Integer.parseInt(scan.nextLine());

        Pawnshop pawnshop = Generate.getPawnshop();
        CreditCafe creditCafe = Generate.getCreditCafe();
        CreditUnion creditUnion = Generate.getCreditUnion();

        float m1 = (pawnshop.credit(amountForCredit) < creditCafe.credit(amountForCredit)) ? pawnshop.credit(amountForCredit) : creditCafe.credit(amountForCredit);
        float m2 = (creditUnion.credit(amountForCredit) < bank.credit(amountForCredit)) ? creditUnion.credit(amountForCredit) : bank.credit(amountForCredit);
        float m3 = (m1 < m2) ? m1 : m2;

        String m = (((pawnshop.credit(amountForCredit) < creditCafe.credit(amountForCredit)) ? pawnshop.credit(amountForCredit) : creditCafe.credit(amountForCredit)) < ((creditUnion.credit(amountForCredit) < bank.credit(amountForCredit)) ? creditUnion.credit(amountForCredit) : bank.credit(amountForCredit)) ?
                ((pawnshop.credit(amountForCredit) < creditCafe.credit(amountForCredit)) ? pawnshop.name : creditCafe.name) : ((creditUnion.credit(amountForCredit) < (bank.credit(amountForCredit)) ? creditUnion.name : bank.name)));

        System.out.println(String.format("%s: %.2f", m, m3));


        System.out.println("Enter the amount of money to deposit(UAH): ");
        int amountForDeposit = Integer.parseInt(scan.nextLine());
        System.out.println("Enter the number of months to deposit: ");
        int mounthForDeposit = Integer.parseInt(scan.nextLine());

        MutualFund mutualFund = Generate.getMutualFund();
        if (mutualFund.deposit(amountForDeposit, mounthForDeposit) > bank.deposit(amountForDeposit, mounthForDeposit)) {
            System.out.println(String.format("%s: %d in %d mounth = %.2f", mutualFund.name, amountForDeposit, mounthForDeposit, mutualFund.deposit(amountForDeposit, mounthForDeposit)));
        } else {
            System.out.println(String.format("%s: %d in %d mounth = %.2f", bank.name, amountForDeposit, mounthForDeposit, bank.deposit(amountForDeposit, mounthForDeposit)));
        }


        System.out.println("Enter the amount to mailing: ");
        int amountForMailing = Integer.parseInt(scan.nextLine());

        PostOffice postOffice = Generate.getPostOffice();
        if (postOffice.mailing(amountForMailing) < bank.mailing(amountForMailing)) {
            System.out.println(String.format("%s: mailing amount(with commission) = %.2f", postOffice.name, postOffice.mailing(amountForMailing)));
        } else {
            System.out.println(String.format("%s: mailing amount(with commission) = %.2f", bank.name, bank.mailing(amountForMailing)));
        }
    }
}
