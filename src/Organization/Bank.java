package Organization;

import interfaceForFinance.Credit;
import interfaceForFinance.CurrencyConverter;
import interfaceForFinance.Deposit;
import interfaceForFinance.Mailing;

import java.util.Map;

public class Bank extends Information implements CurrencyConverter, Credit, Deposit, Mailing {

    int limitForExchange;
    float commission;
    Map<String, Float> currencies;

    int maxAmountForCredit;
    int annualInterestForCredit;

    int interestRateForDeposit;
    int term;

    int commissionForMailing;


    public Bank(String name, String address, int limitForExchange, float commission, Map<String, Float> currencies, int maxAmountForCredit, int annualInterestForCredit, int interestRateForDeposit, int term, int commissionForMailing) {
        super(name, address);
        this.limitForExchange = limitForExchange;
        this.commission = commission;
        this.currencies = currencies;
        this.maxAmountForCredit = maxAmountForCredit;
        this.annualInterestForCredit = annualInterestForCredit;
        this.interestRateForDeposit = interestRateForDeposit;
        this.term = term;
        this.commissionForMailing = commissionForMailing;
    }

    public boolean limit(int amount) {
        return amount < limitForExchange;
    }

    @Override
    public float converter(int amount, String currency) {
        if (limit(amount)) {
            return ((amount - commission) / currencies.get(currency));
        }
        return 0f;
    }

    public boolean limitForCredit(int amountForCredit) {
        return (!(amountForCredit > maxAmountForCredit));
    }

    @Override
    public float credit(int amountForCredit) {
        if (limitForCredit(amountForCredit)) {
            return (amountForCredit + (amountForCredit * annualInterestForCredit) / 100);
        } else {
            return 0f;
        }
    }

    @Override
    public float deposit(int amountForDeposit, int mounthForDeposit) {
        if (!(mounthForDeposit > term)) {
            return (amountForDeposit * interestRateForDeposit / 100 / 12) * mounthForDeposit + amountForDeposit;
        } else {
            System.out.println(String.format("Organization.Bank: the number of months must not exceed: %d", term));
        }
        return 0f;
    }

    @Override
    public float mailing(int amountForMailing) {
        return amountForMailing + (amountForMailing * commissionForMailing) / 100 + 5;

    }
}

