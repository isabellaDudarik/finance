package Organization;

import interfaceForFinance.Credit;

public class CreditUnion extends Information implements Credit {
    int maxAmountForCredit;
    int annualInterestForCredit;

    public CreditUnion(String name, String address, int maxAmountForCredit, int annualInterestForCredit) {
        super(name, address);
        this.maxAmountForCredit = maxAmountForCredit;
        this.annualInterestForCredit = annualInterestForCredit;
    }

    public boolean limitForCredit(int amountForCredit) {
        return (!(amountForCredit > maxAmountForCredit));
    }

    @Override
    public float credit(int amountForCredit) {
        if (limitForCredit(amountForCredit)) {
            return (amountForCredit + (amountForCredit * annualInterestForCredit) / 100);
        } else {
            return 0f;
        }
    }
}
