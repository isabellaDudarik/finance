package Organization;

import interfaceForFinance.CurrencyConverter;

import java.util.Map;

public class Exchanger extends Information implements CurrencyConverter {
    Map<String, Float> currencies;


    public Exchanger(String name, String address, Map<String, Float> currencies) {
        super(name, address);
        this.currencies = currencies;
    }

    @Override
    public float converter(int amount, String currency) {
        return (amount / currencies.get(currency));
    }
}

