package Organization;

public abstract class Information {
    String name;
    String address;

    public Information(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
