package Organization;

import interfaceForFinance.Deposit;

public class MutualFund extends Information implements Deposit {
    int interestRateForDeposit;
    int term;

    public MutualFund(String name, String address, int interestRateForDeposit, int term) {
        super(name, address);
        this.interestRateForDeposit = interestRateForDeposit;
        this.term = term;
    }

    @Override
    public float deposit(int amountForDeposit, int mounthForDeposit) {
        if (!(mounthForDeposit < term)) {
            return (amountForDeposit * interestRateForDeposit / 100 / 12) * mounthForDeposit + amountForDeposit;
        } else {
            System.out.println(String.format("Mutual Fund: the number of months must be at least: %d", term));
        }
        return 0f;
    }
}
