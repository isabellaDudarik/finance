package Organization;

import interfaceForFinance.Mailing;

public class PostOffice extends Information implements Mailing {
    int commissionForMailing;

    public PostOffice(String name, String address, int commissionForMailing) {
        super(name, address);
        this.commissionForMailing = commissionForMailing;
    }

    @Override
    public float mailing(int amountForMailing) {
        return amountForMailing + (amountForMailing*commissionForMailing)/100;
    }
}
