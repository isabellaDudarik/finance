package generated;

import Organization.*;

import java.util.HashMap;
import java.util.Map;

public class Generate {
    public static Bank getBank() {
        Map<String, Float> currencies = new HashMap<>();
        currencies.put("USD", 26.7f);
        currencies.put("EUR", 30.1f);
        currencies.put("RUB", 3.4f);

        return new Bank("Privatbank", "Artema st.", 12_000, 15f, currencies, 200_000, 25, 12, 12, 1);
    }

    public static Exchanger getExchanger() {
        Map<String, Float> currencies = new HashMap<>();
        currencies.put("USD", 25.7f);
        currencies.put("EUR", 29.1f);

        return new Exchanger("Foreign Currency Exchange", "Pushkinska st.", currencies);
    }

    public static Pawnshop getPawnshop() {
        return new Pawnshop("Organization.Pawnshop", "Bluhera st.", 50_000, 40);
    }

    public static CreditCafe getCreditCafe() {
        return new CreditCafe("Organization.CreditCafe", "Sumska st.", 400_000, 200);
    }

    public static CreditUnion getCreditUnion() {
        return new CreditUnion("Organization.CreditUnion", "Pavlova st.", 100_000, 20);
    }

    public static MutualFund getMutualFund() {
        return new MutualFund("Organization.MutualFund", "Naukova st.", 7, 12);
    }
    public static PostOffice getPostOffice(){
        return new PostOffice("Organization.PostOffice", "Pushkinska st.", 2);
    }
}
