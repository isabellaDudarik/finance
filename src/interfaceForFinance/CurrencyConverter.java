package interfaceForFinance;

public interface CurrencyConverter {

    float converter(int amount, String currency);
}
